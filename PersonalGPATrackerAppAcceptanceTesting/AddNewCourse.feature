﻿Feature: Add New Course
	In order to track my GPA
	As a Personal GPA Tracker user
	I want to add my courses

@mytag
Scenario: Add a New Course
	Given I navigate to the home page
	And I have Course List and GPA
	|Code|Title|Credit Hours|Letter Grade|Grade Points|Quality Points|
	And I have issued Add Course command in Home page
	And I have entered CSCI5220 as the code
	And I have entered Soft Ver & Valid as the title
	And I have select 3 as the credit hours
	And I have select A- as the letter grade
	When I click Add Course button
	Then the Course List and GPA shows
	| Code     | Title            | Credit Hours | Letter Grade | Grade Points | Quality Points |
	| CSCI5220 | Soft Ver & Valid | 3            | A-           | 3.7          | 11.1           |
