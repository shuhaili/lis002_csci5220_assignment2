﻿Feature: DeleteCourse
	In order to delete
	As a user
	I want to delete a course

@delete1
Scenario: DeleteCourse
	Given I added a record
	And I click delete button on the record
	When I press confirm button
	Then the record number should be gone
