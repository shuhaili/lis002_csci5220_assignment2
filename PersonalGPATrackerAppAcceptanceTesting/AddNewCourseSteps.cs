﻿using NUnit.Framework;
using PersonalGPATracker.TestingFramework;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using static PersonalGPATracker.TestingFramework.PersonalGPATrackerApp.HomePage;

namespace PersonalGPATrackerAppAcceptanceTesting
{
    [Binding]
    public class AddNewCourseSteps
    {
        [Before]
        public static void Setup()
        {
            PersonalGPATrackerApp.Initialize();
        }

        [After]
        public static void Teardown()
        {
            PersonalGPATrackerApp.EndTest();
        }

        [Given]
        public void GivenINavigateToTheHomePage()
        {
            PersonalGPATrackerApp.HomePage.Goto();
        }
        [Given]
        public void GivenIHaveCourseListAndGPA(Table table)
        {
            if (table.RowCount >= 1)
            {
                Course newCourse = null;

                for (int i = 0; i < table.RowCount; i++)
                {
                    newCourse = new Course(
                    table.Rows[i][0], //Code
                    table.Rows[i][1], //Title
                    Convert.ToInt32(table.Rows[i][2]), //Credit Hours 
                    table.Rows[i][3], //Letter Grade
                    Convert.ToDouble(table.Rows[i][4]), //Grade Points 
                    Convert.ToDouble(table.Rows[i][5])); //Quality Points

                    PersonalGPATrackerApp.CourseList.Add(newCourse);
                }
            }
        }

        [Given]
        public void IHaveIssuedAddCourseCommandInHomePage()
        {
            PersonalGPATrackerApp.HomePage.IssueAddCourseCommand();
        }

        [Given]
        public void GivenIHaveEntered_CODE_AsTheCode(string code)
        {
            PersonalGPATrackerApp.AddCoursePage.Code = code;
        }

        [Given]
        public void GivenIHaveEntered_TITLE_AsTheTitle(string title)
        {
            PersonalGPATrackerApp.AddCoursePage.Title = title;
        }

        [Given]
        public void GivenIHaveSelect_CREDITHOURS_AsTheCreditHours(int creditHours)
        {
            PersonalGPATrackerApp.AddCoursePage.CreditHours = creditHours;
        }

        [Given]
        public void GivenIHaveSelect_LETTERGRADE_AsTheLetterGrade(string letterGrade)
        {
            PersonalGPATrackerApp.AddCoursePage.LetterGrade = letterGrade;
        }

        [When]
        public void WhenIClickAddCourseButton()
        {
            PersonalGPATrackerApp.AddCoursePage.IssueAddCourseCommand();
        }

        [Then]
        public void ThenTheCourseListAndGPAShows(Table table)
        {
            var actual = table.RowCount;
            var expected = PersonalGPATrackerApp.CourseList.Count;

            Assert.That(actual, Is.EqualTo(expected));
            Assert.That(CompareCourseList(table, PersonalGPATrackerApp.CourseList), Is.EqualTo(true));

        }
        public bool CompareCourseList(Table table, List<Course> courseList)
        {
            Course tmpCourse = null;
            for(int i=0;  i<table.RowCount; i++)
            {
                tmpCourse = new Course(table.Rows[i][0],
                    table.Rows[i][1],
                    Convert.ToInt32(table.Rows[i][2]),
                    table.Rows[i][3],
                    Convert.ToDouble(table.Rows[i][4]),
                    Convert.ToDouble(table.Rows[i][5]));
                if (!tmpCourse.CompareRecord(courseList[i]))
                    return false;
            }

            return true;
        }
    }
}
