﻿using System;
using TechTalk.SpecFlow;
using PersonalGPATracker.TestingFramework;
using NUnit.Framework;

namespace PersonalGPATrackerAppAcceptanceTesting
{
    [Binding]
    public class TitleBarFeatureInAllPagesSteps
    {
        [Before]
        public static void Setup()
        {
            PersonalGPATrackerApp.Initialize();
        }

        [After]
        public static void Teardown()
        {
            PersonalGPATrackerApp.EndTest();
        }

        [Given]
        public void GivenIAmOnThePersonalGPATrackerHomePage()
        {
            PersonalGPATrackerApp.HomePage.Goto();
        }

        [When]
        public void WhenIIssueAddCourseCommand()
        {
            PersonalGPATrackerApp.HomePage.IssueAddCourseCommand();
        }

        [Then]
        public void ThenTheAddCoursePageShows_APPNAME_AsApplicationNameAnd_HOMEPAGE_AsHomePageLinkAnd_ADDCOURSE_AsAddNewCoursePageAnd_DEVELOPERNAME_AsDeveloperNameAnd_DEVELOPEREMAIL_AsDeveloperEmail(String appName, String homePage, String addCourse, String developerName, String developerEmail)
        {
            var actualAppName = PersonalGPATrackerApp.HomePage.AppName;
            var actualHomePageName = PersonalGPATrackerApp.HomePage.HomePageName;
            var actualAddCourseName = PersonalGPATrackerApp.HomePage.AddCoursePageName;
            var actualDeveloperName = PersonalGPATrackerApp.HomePage.DeveloperName;
            var actualDeveloperEmail = PersonalGPATrackerApp.HomePage.DeveloperEmail;

            Assert.That(actualAppName, Is.EqualTo(appName));
            Assert.That(actualHomePageName, Is.EqualTo(homePage));
            Assert.That(actualAddCourseName, Is.EqualTo(addCourse));
            Assert.That(actualDeveloperName, Is.EqualTo(developerName));
            Assert.That(actualDeveloperEmail, Is.EqualTo(developerEmail));
        }

        [Given]
        public void GivenINavigateToThePersonalGPATrackerHomePage()
        {
            PersonalGPATrackerApp.HomePage.Goto();
        }
        
        [Then]
        public void ThenTheHomePageShows_APPNAME_AsApplicationNameAnd_HOMEPAGE_AsHomePageLinkAnd_ADDCOURSE_AsAddNewCoursePageAnd_DEVELOPERNAME_AsDeveloperNameAnd_DEVELOPEREMAIL_AsDeveloperEmail(String appName, String homePage, String addCourse, String developerName, String developerEmail)
        {
            var actualAppName = PersonalGPATrackerApp.HomePage.AppName;
            var actualHomePageName = PersonalGPATrackerApp.HomePage.HomePageName;
            var actualAddCourseName = PersonalGPATrackerApp.HomePage.AddCoursePageName;
            var actualDeveloperName = PersonalGPATrackerApp.HomePage.DeveloperName;
            var actualDeveloperEmail = PersonalGPATrackerApp.HomePage.DeveloperEmail;

            Assert.That(actualAppName, Is.EqualTo(appName));
            Assert.That(actualHomePageName, Is.EqualTo(homePage));
            Assert.That(actualAddCourseName, Is.EqualTo(addCourse));
            Assert.That(actualDeveloperName, Is.EqualTo(developerName));
            Assert.That(actualDeveloperEmail, Is.EqualTo(developerEmail));
        }
    }
}
