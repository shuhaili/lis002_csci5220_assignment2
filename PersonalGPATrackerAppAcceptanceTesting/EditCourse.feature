﻿Feature: Edit a Course
	In order to change a course
	As a user
	I want to edit my courses

@edit1
Scenario: Edit a Course
	Given I am on the homepage
	And I click Add-Course button on Home page
	And I enter code title credit hour lettergrade
	And I click Add Course button
	And I click Edit on a record
	And I change title and click update
	Then the change should show
