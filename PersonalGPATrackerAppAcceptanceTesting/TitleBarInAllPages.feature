﻿Feature: Title Bar Feature in all Pages
	In order to verify the title bar feature
	As a Personal GPA Tracker user
	I want to see the title bar in all pages

@Title Bar
Scenario: Title Bar in Home Page
	Given I navigate to the Personal GPA Tracker Home Page
	Then the Home page shows Personal GPA Tracker as application name and Home as home page link and Add Course as add new course page and Jeff Roach as developer name and roachj@etsu.edu as developer email

@Title Bar
Scenario: Title Bar in Add Couse Page
	Given I am on the Personal GPA Tracker Home page
	When I issue Add Course Command
	Then the Add Course page shows Personal GPA Tracker as application name and Home as home page link and Add Course as add new course page and Jeff Roach as developer name and roachj@etsu.edu as developer email