﻿using NUnit.Framework;
using PersonalGPATracker.TestingFramework;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using static PersonalGPATracker.TestingFramework.PersonalGPATrackerApp.HomePage;
using System.Threading;

namespace PersonalGPATrackerAppAcceptanceTesting
{
    [Binding]
    public class ShowCourseAndGPASteps
    {
        [Before]
        public static void Setup()
        {
            PersonalGPATrackerApp.Initialize();
        }

        [After]
        public static void Teardown()
        {
            PersonalGPATrackerApp.EndTest();
        }

        [Given]
        public void GivenINavigateToHomePage()
        {
            PersonalGPATrackerApp.HomePage.Goto();

        }
        
        [Given]
        public void GivenIAddACourse()
        {
            PersonalGPATrackerApp.HomePage.IssueAddCourseCommand();
            Thread.Sleep(1000);
            PersonalGPATrackerApp.AddCoursePage.Code = "CSCI2020";
            PersonalGPATrackerApp.AddCoursePage.Title = "Database";
            PersonalGPATrackerApp.AddCoursePage.CreditHours = 3;
            PersonalGPATrackerApp.AddCoursePage.LetterGrade = "A";
            PersonalGPATrackerApp.AddCoursePage.IssueAddCourseCommand();         
        }
        
        [Given]
        public void GivenIAddAnotherCourse()
        {
            PersonalGPATrackerApp.HomePage.IssueAddCourseCommand();
            Thread.Sleep(1000);
            PersonalGPATrackerApp.AddCoursePage.Code = "CSCI5220";
            PersonalGPATrackerApp.AddCoursePage.Title = "SoftwareV&V";
            PersonalGPATrackerApp.AddCoursePage.CreditHours = 3;
            PersonalGPATrackerApp.AddCoursePage.LetterGrade = "B";
            PersonalGPATrackerApp.AddCoursePage.IssueAddCourseCommand();
            Thread.Sleep(1000);
        }
        
        [Then]
        public void ThenGPAShouldBeShownOnThePage()
        {
            double expectedGPA = 3.5;
            Thread.Sleep(1000);
            double actualGPA = PersonalGPATrackerApp.HomePage.GetGPA();
            Assert.That(expectedGPA, Is.EqualTo(actualGPA));
        }
    }
}
