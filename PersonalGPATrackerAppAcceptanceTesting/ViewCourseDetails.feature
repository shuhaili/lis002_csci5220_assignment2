﻿Feature: ViewCourseDetails
	In order to view details
	As an user
	I want to be able to view details of course information

@viewDetails
Scenario: viewcourDetails 
	Given navigate to homepage
	And I add record
	When I press view button on a record
	Then the details should be displayed on screen
