﻿using NUnit.Framework;
using PersonalGPATracker.TestingFramework;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using static PersonalGPATracker.TestingFramework.PersonalGPATrackerApp.HomePage;
using System.Threading;

namespace PersonalGPATrackerAppAcceptanceTesting
{
    [Binding]
    public class DeleteCourseSteps
    {
        [Before]
        public static void Setup()
        {
            PersonalGPATrackerApp.Initialize();
        }

        [After]
        public static void Teardown()
        {
            PersonalGPATrackerApp.EndTest();
        }

        [Given]
        public void GivenIAddedARecord()
        {
            PersonalGPATrackerApp.HomePage.Goto();
            PersonalGPATrackerApp.HomePage.IssueAddCourseCommand();
            PersonalGPATrackerApp.AddCoursePage.Code = "CSCI2020";
            PersonalGPATrackerApp.AddCoursePage.Title = "Database";
            PersonalGPATrackerApp.AddCoursePage.CreditHours = 3;
            PersonalGPATrackerApp.AddCoursePage.LetterGrade = "A";
            PersonalGPATrackerApp.AddCoursePage.IssueAddCourseCommand();
        }
        
        [Given]
        public void GivenIClickDeleteButtonOnTheRecord()
        {
            PersonalGPATrackerApp.HomePage.IssueDeleteCourseCommand();
        }
        
        [When]
        public void WhenIPressConfirmButton()
        {
            PersonalGPATrackerApp.DeleteCoursePage.ConfirmDeleteCourse();
        }
        
        [Then]
        public void ThenTheRecordNumberShouldBeGone()
        {
            var numberRecord = 0;
            var actualNumberRecord=PersonalGPATrackerApp.DeleteCoursePage.GetRecordNumber();
            Assert.That(numberRecord, Is.EqualTo(actualNumberRecord));
        }
    }
}
