﻿using NUnit.Framework;
using PersonalGPATracker.TestingFramework;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using static PersonalGPATracker.TestingFramework.PersonalGPATrackerApp.HomePage;
using System.Threading;

namespace PersonalGPATrackerAppAcceptanceTesting
{
    [Binding]
    public class ViewCourseDetailsSteps
    {
        [Before]
        public static void Setup()
        {
            PersonalGPATrackerApp.Initialize();
        }

        [After]
        public static void Teardown()
        {
            PersonalGPATrackerApp.EndTest();
        }

        [Given]
        public void GivenNavigateToHomepage()
        {
            PersonalGPATrackerApp.HomePage.Goto();
        }
        
        [Given]
        public void GivenIAddRecord()
        {
            PersonalGPATrackerApp.HomePage.IssueAddCourseCommand();
            Thread.Sleep(1000);
            PersonalGPATrackerApp.AddCoursePage.Code = "CSCI2020";
            PersonalGPATrackerApp.AddCoursePage.Title = "Database";
            PersonalGPATrackerApp.AddCoursePage.CreditHours = 3;
            PersonalGPATrackerApp.AddCoursePage.LetterGrade = "A";
            PersonalGPATrackerApp.AddCoursePage.IssueAddCourseCommand();
        }
        
        [When]
        public void WhenIPressViewButtonOnARecord()
        {
            PersonalGPATrackerApp.HomePage.IssueViewCourseDetialsCommand();
        }
        
        [Then]
        public void ThenTheDetailsShouldBeDisplayedOnScreen()
        {
            var actualCode = PersonalGPATrackerApp.ViewDetailsPage.Code;
            var actualTitle = PersonalGPATrackerApp.ViewDetailsPage.Title;
            var actualCreditHours = PersonalGPATrackerApp.ViewDetailsPage.CreditHours;
            var actualLetterGrade = PersonalGPATrackerApp.ViewDetailsPage.LetterGrade;
            Assert.That(actualCode, Is.EqualTo("CSCI2020"));
            Assert.That(actualTitle, Is.EqualTo("Database"));
            Assert.That(actualCreditHours, Is.EqualTo("3"));
            Assert.That(actualLetterGrade, Is.EqualTo("A"));
        }
    }
}
