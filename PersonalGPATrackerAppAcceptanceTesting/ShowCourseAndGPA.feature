﻿Feature: ShowCourseAndGPA
	In order to show course information
	As a user
	I want to view course information

@mytag
Scenario: ShowCourseAndGPA
	Given I navigate to home page
	And I add a course
	And I add another Course
	Then GPA should be shown on the page
