﻿using PersonalGPATracker.WebDriverFramework;
using System;
using System.Collections.Generic;
using static PersonalGPATracker.TestingFramework.PersonalGPATrackerApp.HomePage;

namespace PersonalGPATracker.TestingFramework
{

    public class Course
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public int CreditHours { get; set; }
        public string LetterGrade { get; set; }
        public double GradePoints { get; set; }
        public double QualityPoints { get; set; }

        public Course(string code, string title, int creditHours, string letterGrade, double gradePoints, double qualityPoints)
        {
            Code = code;
            Title = title;
            CreditHours = creditHours;
            LetterGrade = letterGrade;
            GradePoints = gradePoints;
            QualityPoints = qualityPoints;
        }

        public bool CompareRecord(Course course)
        {
            if (course.Code.CompareTo(Code) == 0
                && course.Title.CompareTo(Title) == 0
                && course.LetterGrade.CompareTo(LetterGrade) == 0
                && course.GradePoints == GradePoints
                && Math.Abs(course.QualityPoints - QualityPoints) < 0.001)
                return true;
            return false;
        }
    }

    public static class PersonalGPATrackerApp
    {
        private const string HostUrl = "http://localhost:58316/";
        private const string homeUrl = HostUrl;

        public static List<Course> CourseList = new List<Course>(6);

        public static void Initialize()
        {
            Chrome.Initialize();
        }

        public static void EndTest()
        {
            Chrome.Quit();
        }

        public static class HomePage
        {
            public static string AppName
            {
                get { return Chrome.AppName; }
            }

            public static string HomePageName
            {
                get { return Chrome.HomePageName; }
            }

            public static string AddCoursePageName
            {
                get { return Chrome.AddCoursePageName; }
            }

            public static string DeveloperName
            {
                get { return Chrome.DeveloperName; }
            }

            public static string DeveloperEmail
            {
                get { return Chrome.DeveloperEmail; }
            }

            public static void Goto()
            {
                Chrome.Goto(homeUrl);
            }

            public static void IssueAddCourseCommand()
            {
                Chrome.ClickAddCourseLink();
            }

            public static void IssueEditCourseCommand()
            {
                Chrome.ClickEditCourseLink();
            }

            public static void IssueDeleteCourseCommand()
            {
                Chrome.ClickDeleteCourseLink();
            }
            public static void IssueViewCourseDetialsCommand()
            {
                Chrome.ClickViewCourseLink();
            }
            public static double GetGPA()
            {
                return Chrome.GetGPA();
            }

        }

        public static class AddCoursePage
        {
            public static void IssueAddCourseCommand()
            {
                CourseList.Add(new Course(Code, Title, CreditHours, LetterGrade, GradePoints, QualityPoints));
                Chrome.ClickAddCourseButton();
            }

            public static string Code
            {
                get
                {
                    return Chrome.Code;
                }
                set
                {
                    Chrome.Code = value;
                }
            }
            public static string Title
            {
                get
                {
                    return Chrome.Title;
                }
                set
                {
                    Chrome.Title = value;
                }

            }
            public static int CreditHours
            {
                get
                {
                    return Convert.ToInt32(Chrome.CreditHours);
                }
                set
                {
                    Chrome.CreditHours = value + "";
                }
            }
            public static string LetterGrade
            {
                get
                {
                    return Chrome.LetterGrade;
                }
                set
                {
                    Chrome.LetterGrade = value;
                }
            }
            public static double GradePoints
            {
                get
                {
                    return GradePoint.GetGradePoints(Chrome.LetterGrade);
                }
            }
            public static double QualityPoints
            {
                get
                {
                    return Convert.ToInt32(Chrome.CreditHours) * GradePoint.GetGradePoints(Chrome.LetterGrade);
                }
            }
        }
            /*----------------------------------------------------*/
        public static class EditCoursePage
        {
            public static void Update()
            {
                //CourseList.Add(new Course(Code, Title, CreditHours, LetterGrade, GradePoints, QualityPoints));
                CourseList[0].Title = EditCoursePage.Title;
                Chrome.ClickUpdate();
            }
            public static string Code
            {
                get
                {
                    return Chrome.Code;
                }
                set
                {
                    Chrome.Code = value;
                }
            }
            public static string Title
            {
                get
                {
                    return Chrome.ReturnTitleValue();
                }
                set
                {
                    Chrome.ChangeTitle(value);
                }

            }
            public static int CreditHours
            {
                get
                {
                    return Convert.ToInt32(Chrome.CreditHours);
                }
                set
                {
                    Chrome.CreditHours = value + "";
                }
            }
            public static string LetterGrade
            {
                get
                {
                    return Chrome.LetterGrade;
                }
                set
                {
                    Chrome.LetterGrade = value;
                }
            }
            public static double GradePoints
            {
                get
                {
                    return GradePoint.GetGradePoints(Chrome.LetterGrade);
                }
            }
            public static double QualityPoints
            {
                get
                {
                    return Convert.ToInt32(Chrome.CreditHours) * GradePoint.GetGradePoints(Chrome.LetterGrade);
                }
            }
        }

        public static class DeleteCoursePage
        {
            public static void ConfirmDeleteCourse()
            {
                CourseList.RemoveAt(0);
                Chrome.ConfirmDelete();
            }

            public static int GetRecordNumber()
            {
                return Chrome.GetRecordNumber();
            }
        }

        public static class ViewDetailsPage
        {
            public static string Code
            {
                get
                {
                    return Chrome.getViewCode();
                }
                
            }
            public static string Title
            {
                get
                {
                    return Chrome.getViewTitle();
                }
            }

            public static string CreditHours
            {
                get
                {
                    return Chrome.getViewCreditHours();
                }
            }
            public static string LetterGrade
            {
                get
                {
                    return Chrome.getViewLetterGrade();
                }
            }
        }
        
    }
}
