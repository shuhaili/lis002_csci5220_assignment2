﻿namespace PersonalGPATracker.TestingFramework
{
    public static class GradePoint
    {
        public static double GetGradePoints(string letterGrade)
        {
            double gradePoints = 0.0;
            switch (letterGrade)
            {
                case "A":
                    gradePoints = 4.0;
                    break;
                case "A-":
                    gradePoints = 3.7;
                    break;
                case "B+":
                    gradePoints = 3.3;
                    break;
                case "B":
                    gradePoints = 3.0;
                    break;
                case "B-":
                    gradePoints = 2.7;
                    break;
                case "C+":
                    gradePoints = 2.3;
                    break;
                case "C":
                    gradePoints = 2.0;
                    break;
                case "C-":
                    gradePoints = 1.7;
                    break;
                case "D+":
                    gradePoints = 1.3;
                    break;
                case "D":
                    gradePoints = 1.0;
                    break;
                case "F":
                    gradePoints = 0.0;
                    break;
                case "U":
                    gradePoints = 0.0;
                    break;
            }

            return gradePoints;
        }
    }
}
