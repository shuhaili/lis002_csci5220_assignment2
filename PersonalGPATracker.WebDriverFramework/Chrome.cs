﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace PersonalGPATracker.WebDriverFramework
{
    public static class Chrome
    {
        private static IWebDriver _page = null;

        public static void Initialize()
        {
            if(_page == null)
                _page = new ChromeDriver(@"G:\shuhai-1-3-2016\temp-temp\computer course\software validation and verification###\projects\PersonalGPATrackerTest2\PersonalGPATracker.WebDriverFramework");
        }

        /*--------------------------- Home Page--------------------------------------*/
        public static string AppName
        {
            get
            {
                return _page.FindElement(By.XPath(@"/html/body/div[1]/div/div[1]/a")).Text;
            }
        }

        public static string HomePageName
        {
            get
            {
                return _page.FindElement(By.XPath(@"/html/body/div[1]/div/div[2]/ul/li[1]/a")).Text;
            }
        }

        public static string AddCoursePageName
        {
            get
            {
                return _page.FindElement(By.XPath(@"/html/body/div[1]/div/div[2]/ul/li[2]/a")).Text;
            }
        }

        public static string DeveloperName
        {
            get
            {
                return _page.FindElement(By.XPath(@"/html/body/div[1]/div/div[2]/ul/li[3]/a")).Text;
            }
        }

        public static string DeveloperEmail
        {
            get
            {
                return _page.FindElement(By.XPath(@"/html/body/div[1]/div/div[2]/ul/li[4]/a")).Text;
            }
        }

        public static double GetGPA()
        {
            double gpa;
            string a = _page.FindElement(By.XPath(@"/html/body/div[2]/h1")).Text;
            gpa = Convert.ToDouble(a.Substring(11, 4));
            return gpa;
        }
        /*--------------------------- For Add Course -----------------------------------*/

        public static string Code
        {
            set
            {
                var codeElement = _page.FindElement(By.XPath("//*[@id=\"Code\"]"));
                codeElement.Clear();
                codeElement.SendKeys(value);
            }
            get
            {
                return _page.FindElement(By.Id("Code")).GetAttribute("value");
            }
        }

        public static string Title
        {
            set
            {
                var titleElement = _page.FindElement(By.XPath("//*[@id=\"Title\"]"));
                titleElement.Clear();
                titleElement.SendKeys(value);
            }
            get
            {
                return _page.FindElement(By.Id("Title")).GetAttribute("value");
            }
        }

        public static string CreditHours
        {
            set
            {
                var dropdownCreditHours = new SelectElement(_page.FindElement(By.XPath("//*[@id=\"CreditHours\"]")));
                dropdownCreditHours.SelectByText(value);
            }
            get
            {
                var dropdownCreditHours = new SelectElement(_page.FindElement(By.XPath("//*[@id=\"CreditHours\"]")));
                return dropdownCreditHours.SelectedOption.Text;
            }
        }

        public static string LetterGrade
        {
            set
            {
                var dropdownLetterGrade = new SelectElement(_page.FindElement(By.XPath("//*[@id=\"LetterGrade\"]")));
                dropdownLetterGrade.SelectByText(value);
            }
            get
            {
                var dropdownLetterGrade = new SelectElement(_page.FindElement(By.XPath("//*[@id=\"LetterGrade\"]")));
                return dropdownLetterGrade.SelectedOption.Text;
            }
        }

        public static void ClickAddCourseLink()
        {
            _page.FindElement(By.XPath(@"/html/body/div[1]/div/div[2]/ul/li[2]/a")).Click();
        }

        public static void ClickAddCourseButton()
        {
            _page.FindElement(By.XPath(@"/html/body/div[2]/form/div/div[5]/div/input")).Click();
        }

        public static void Quit()
        {
            Thread.Sleep(1000);
            _page.Dispose();
            _page.Quit();
        }

        public static void Goto(string url)
        {
            _page.Navigate().GoToUrl(url);
        }

        /*------------------Edit Course----------------------------*/
        public static void ClickEditCourseLink()
        {
            _page.FindElement(By.XPath(@"/html/body/div[2]/table/tbody/tr[2]/td[7]/a[1]")).Click();
        }

        /*------------------View Course----------------------------*/
        public static void ClickViewCourseLink()
        {
            _page.FindElement(By.XPath(@"/html/body/div[2]/table/tbody/tr[2]/td[7]/a[2]")).Click();
        }

        public static string getViewCode()
        {
            return _page.FindElement(By.XPath(@"/html/body/div[2]/div/dl/dd[1]")).Text;
        }
        public static string getViewTitle()
        {
            return _page.FindElement(By.XPath(@"/html/body/div[2]/div/dl/dd[2]")).Text;
        }

        public static string getViewCreditHours()
        {
            return _page.FindElement(By.XPath(@"/html/body/div[2]/div/dl/dd[3]")).Text;
        }

        public static string getViewLetterGrade()
        {
            return _page.FindElement(By.XPath(@"/html/body/div[2]/div/dl/dd[4]")).Text;
        }


        /*------------------Delete Course----------------------------*/
        public static void ClickDeleteCourseLink()
        {
            _page.FindElement(By.XPath(@"/html/body/div[2]/table/tbody/tr[2]/td[7]/a[3]")).Click();
        }

        public static void ChangeTitle(string value)
        {
            var titleElement = _page.FindElement(By.XPath("//*[@id=\"Title\"]"));
            titleElement.Clear();
            titleElement.SendKeys(value);
        }

        public static void ClickUpdate()
        {
            _page.FindElement(By.XPath(@"/html/body/div[2]/form/div/div[5]/div/input")).Click();
            Thread.Sleep(3000);
        }

        public static string ReturnTitleValue()
        {
            //return  _page.FindElement(By.XPath("/html/body/div[2]/table/tbody/tr[2]/td[2]")).GetAttribute("value");
            return _page.FindElement(By.Id("Title")).GetAttribute("value");
        }

        /*------------------Delete Course----------------------------*/

        public static void ConfirmDelete()
        {
            Thread.Sleep(1000);
            _page.FindElement(By.XPath(@"/html/body/div[2]/div/form/div/input[2]")).Click();
        }

        public static int GetRecordNumber()
        {
            int i;
            i = Convert.ToInt32(_page.FindElement(By.ClassName("table")).GetAttribute("childElementCount"))-1;   
            return i;
        }


    }
}
